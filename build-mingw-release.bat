SET QTDIR=%cd%
SET PREFIX=%QTDIR%\qtbase

SET PATH=C:\Qt\icu.mingw\bin;C:\Qt\icu.mingw\lib;%PATH%
SET PATH=C:\Program Files (x86)\Ruby193\bin;%PATH%
SET PATH=C:\strawberry\perl\bin;C:\strawberry\c\bin;%PATH%
SET PATH=C:\Qt\src.git\gnuwin32\bin;C:\MinGW\bin;C:\MinGw\opt\bin;%PATH%
SET PATH=C:\MinGW\opt\lib;%PREFIX%\bin;%PATH%
                                           
SET INCLUDE=C:\Qt\icu.mingw\include;C:\MinGW\opt\include;%INCLUDE%
SET LIB=C:\Qt\icu.mingw\lib;C:\MinGw\opt\bin;C:\MinGW\opt\lib;%LIB%

call ..\src.git\configure.bat -release -opensource -prefix %PREFIX% -c++std c++14 -confirm-license -platform win32-g++ -opengl desktop -nomake examples -nomake tests -no-feature-geoservices_mapboxgl -skip qtwebengine -I C:\Qt\icu.mingw\include -I C:\MinGW\opt\include -L C:\Qt\icu.mingw\lib -L C:\MinGw\opt\bin -L C:\MinGW\opt\lib

mingw32-make -j6
mingw32-make docs

