#!/bin/bash

export QTDIR=$PWD
export PATH=$QTDIR/qtbase/bin:$PATH
export PREFIX=$PWD/qtbase
export OPENSSL=$PWD/openssl

SDKVERSION=`xcrun -sdk macosx --show-sdk-version`
DEVELOPER=`xcode-select -print-path`

if [ ! -d "$DEVELOPER" ]; then
  echo "xcode path is not set correctly $DEVELOPER does not exist (most likely because of xcode > 4.3)"
  echo "run"
  echo "sudo xcode-select -switch <xcode path>"
  echo "for default installation:"
  echo "sudo xcode-select -switch /Applications/Xcode.app/Contents/Developer"
  exit 1
fi

PLATFORM="MacOSX"
TOOLCHAIN=`xcrun -sdk macosx -find cc`
TOOLCHAIN=$(dirname ${TOOLCHAIN})

export CROSS_TOP="${DEVELOPER}/Platforms/${PLATFORM}.platform/Developer"
export CROSS_SDK="${PLATFORM}${SDKVERSION}.sdk"
export BUILD_TOOLS="${TOOLCHAIN}"

cd `dirname $0`/src.openssl

export MACOS_DEPLOYMENT_TARGET=10.10
export MACOS_SYSROOT="${DEVELOPER}/Platforms/${PLATFORM}.platform/Developer/SDKs/${PLATFORM}${SDKVERSION}.sdk"
export CC=${TOOLCHAIN}/cc

#
# NOTE: Openssl Configure depends on system environment variables:
# MACOS_DEPLOYMENT_TARGET MACOS_SYSROOT CC
# They are have to be set before Configure
#
./Configure darwin64-x86_64-cc --prefix=$OPENSSL-x86_64 --openssldir=$OPENSSL-x86_64 no-symlink no-asm
make clean
make
make install
make clean

export -n MACOS_DEPLOYMENT_TARGET
export -n MACOS_SYSROOT
export -n CC

mkdir -p "${PREFIX}/lib"
${TOOLCHAIN}/lipo -create ${OPENSSL}-x86_64/lib/libssl.a  -output ${PREFIX}/lib/libssl.a
${TOOLCHAIN}/lipo -create ${OPENSSL}-x86_64/lib/libcrypto.a -output ${PREFIX}/lib/libcrypto.a
mkdir -p ${PREFIX}/include
cp -R ${OPENSSL}-x86_64/include ${PREFIX}/

cd $QTDIR
echo "Build library..."

#
# IOS sdk compiller support only c++14 dialect.
# So we make it consistent across all supported platforms.
#

`dirname $0`/src.git/configure -prefix $PREFIX -debug-and-release -c++std c++14 \
  -sdk macosx -opensource -confirm-license -nomake examples -nomake tests \
  -no-feature-geoservices_mapboxgl -no-feature-assimp -openssl-linked -I $PREFIX/include -L $PREFIX/lib

make -j7
make docs

echo -e "\nall done."

-debug-and-release -c++std c++14 -sdk macosx -opensource -confirm-license -nomake examples -nomake tests
