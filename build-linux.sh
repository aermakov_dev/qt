#!/bin/bash

export QTDIR=$PWD
export PATH=$QTDIR/qtbase/bin:$PATH
export PREFIX=$PWD/qtbase

#
# IOS sdk compiller support only c++14 dialect.
# So we make it consistent across all supported platforms.
#

`dirname $0`/src.git/configure -platform linux-g++-64 -prefix $PREFIX -c++std c++14 \
  -opensource -confirm-license -nomake examples -nomake tests -no-feature-geoservices_mapboxgl \
  -v

make -j4
make docs

echo -e "\nall done."