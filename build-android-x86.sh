#!/bin/bash

export QTDIR=$PWD
export PATH=$QTDIR/qtbase/bin:$PATH
export PREFIX=$PWD/qtbase

export ANDROID_NDK_ROOT=/Volumes/Store/Work/android/sdk/ndk-bundle
export ANDROID_SDK_ROOT=/Volumes/Store/Work/android/sdk
export ANDROIN_TOOLCHAIN_VERSION=4.9
export ANDROID_API=18
export ANDROID_API_VERSION=android-$ANDROID_API
export ANDROID_NDK_PLATFORM=android-$ANDROID_API
export ANDROID_NDK_HOST=darwin-x86_64
export ANDROID_ARCH=x86

cd `dirname $0`/src.openssl

BUILD_PATH=$PATH
export PATH=$ANDROID_NDK_ROOT/toolchains/x86-$ANDROIN_TOOLCHAIN_VERSION/prebuilt/$ANDROID_NDK_HOST/bin:$PATH
export PATH=$ANDROID_NDK_ROOT/toolchains/llvm/prebuilt/$ANDROID_NDK_HOST/bin:$PATH
export CC=clang
export AR=i686-linux-android-ar
export RANLIB=i686-linux-android-ranlib

#
# NOTE: Openssl Configure depends on system environment variables:
# ANDROID_API ANDROID_NDK_ROOT ANDROID_NDK_PLATFORM ANDROID_NDK_HOST ANDROIN_TOOLCHAIN_VERSION CC AR RANLIB
# They are have to be set before Configure
#
./Configure shared android-x86 --prefix=$PREFIX --openssldir=$PREFIX no-symlink no-asm -D__ANDROID_API__=$ANDROID_API

make clean
make build_libs
make install
make clean

export -n CC
export -n AR
export -n RANLIB
export PATH=$BUILD_PATH

cd $QTDIR

#
# IOS sdk compiller support only c++14 dialect.
# So we make it consistent across all supported platforms.
#

`dirname $0`/src.git/configure -xplatform android-clang -prefix $PREFIX -c++std c++14 \
  -android-ndk $ANDROID_NDK_ROOT -android-sdk $ANDROID_SDK_ROOT \
  -android-ndk-host $ANDROID_NDK_HOST -android-toolchain-version $ANDROIN_TOOLCHAIN_VERSION \
  -android-ndk-platform $ANDROID_NDK_PLATFORM -android-arch $ANDROID_ARCH \
  -opensource -confirm-license -nomake examples -nomake tests -skip qtwebengine \
  -skip qtserialport -skip qtwebkit -skip speech -skip connectivity -no-feature-geoservices_mapboxgl -no-feature-assimp \
  -I $PREFIX/include -L $PREFIX/lib

make -j7

ANDROID_STRIP=$ANDROID_NDK_ROOT/toolchains/llvm/prebuilt/$ANDROID_NDK_HOST/bin/i686-linux-android-strip
echo "Strip libraries:"
for lib in `find ./qtbase -name *.so`
do
    echo "$lib"
    $ANDROID_STRIP --strip-unneeded $lib
done

echo -e "\nall done."
