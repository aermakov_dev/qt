SET QTDIR=%cd%
SET PREFIX=%QTDIR%\qtbase

SET PATH=C:\strawberry\perl\bin;C:\Qt\src.git\gnuwin32\bin;%QTDIR%\bin;%PATH%
SET PATH=C:\Ruby193\bin;%PATH%
SET PATH=C:\Qt\icu.win64\bin64;%PATH%
SET PATH=%QTDIR%\bin;%PATH%

call ..\src.git\configure.bat -debug-and-release -prefix %PREFIX% -opensource -confirm-license -platform win32-msvc -mp -opengl desktop -nomake examples -nomake tests -skip qtwebengine -no-feature-geoservices_mapboxgl -I "C:\Qt\icu.win64\include" -L "C:\Qt\icu.win64\lib64" -I "C:\Program Files\OpenSSL-Win64\include" -L "C:\Program Files\OpenSSL-Win64\lib"

nmake

