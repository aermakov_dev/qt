SET QTDIR=%cd%
SET PREFIX=%QTDIR%\qtbase

SET PATH=C:\strawberry\perl\bin;C:\Qt\src.git\gnuwin32\bin;%QTDIR%\bin;%PATH%
SET PATH=C:\Program Files (x86)\Ruby193\bin;%PATH%
SET PATH=C:\Qt\icu.win32\bin;%PATH%
SET PATH=%QTDIR%\bin;%PATH%

call ..\src.git\configure.bat -debug-and-release -prefix %PREFIX% -opensource -confirm-license -platform win32-msvc -mp -opengl desktop -nomake examples -nomake tests -skip qtwebengine -no-feature-geoservices_mapboxgl -I "C:\Qt\icu.win32\include" -L "C:\Qt\icu.win32\lib" -I "C:\Program Files (x86)\OpenSSL-Win32\include" -L "C:\Program Files (x86)\OpenSSL-Win32\lib"

nmake 
nmake docs

