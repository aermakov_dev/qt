#!/bin/bash

export QTDIR=$PWD
export PATH=$QTDIR/qtbase/bin:$PATH
export PREFIX=$PWD/qtbase
export OPENSSL=$PWD/openssl

SDKVERSION=`xcrun -sdk iphoneos --show-sdk-version`
DEVELOPER=`xcode-select -print-path`

if [ ! -d "$DEVELOPER" ]; then
  echo "xcode path is not set correctly $DEVELOPER does not exist (most likely because of xcode > 4.3)"
  echo "run"
  echo "sudo xcode-select -switch <xcode path>"
  echo "for default installation:"
  echo "sudo xcode-select -switch /Applications/Xcode.app/Contents/Developer"
  exit 1
fi

cd `dirname $0`/src.openssl

ARCHS="armv7 arm64"
PLATFORM="iPhoneOS"
TOOLCHAIN=`xcrun -sdk iphoneos -find cc`
TOOLCHAIN=$(dirname ${TOOLCHAIN})

export IOS_DEPLOYMENT_TARGET=9.3
export IOS_SYSROOT="${DEVELOPER}/Platforms/${PLATFORM}.platform/Developer/SDKs/${PLATFORM}${SDKVERSION}.sdk"
export CROSS_COMPILE="${TOOLCHAIN}/"

for ARCH in ${ARCHS}
do
    export CC="cc -arch ${ARCH}"
    #
    # NOTE: Openssl Configure depends on system environment variables:
    # IOS_DEPLOYMENT_TARGET IOS_SYSROOT CROSS_COMPILE CC
    # They are have to be set before Configure
    #
    ./Configure iphoneos-cross --prefix=$OPENSSL-${ARCH} --openssldir=$OPENSSL-${ARCH} no-symlink no-asm
    make clean
    make
    make install
    make clean
    export -n CC
done

PLATFORM="iPhoneSimulator"
TOOLCHAIN=`xcrun -sdk iphonesimulator -find cc`
TOOLCHAIN=$(dirname ${TOOLCHAIN})

export IOS_DEPLOYMENT_TARGET=9.3
export IOS_SYSROOT="${DEVELOPER}/Platforms/${PLATFORM}.platform/Developer/SDKs/${PLATFORM}${SDKVERSION}.sdk"
export CROSS_COMPILE="${TOOLCHAIN}/"
export CC="cc -arch x86_64"
#
# NOTE: Openssl Configure depends on system environment variables:
# IOS_DEPLOYMENT_TARGET IOS_SYSROOT CROSS_COMPILE CC
# They are have to be set before Configure
#
./Configure iphoneos-cross --prefix=$OPENSSL-x86_64 --openssldir=$OPENSSL-x86_64 no-symlink no-asm
make clean
make
make install
make clean
export -n CC

mkdir -p "${PREFIX}/lib"
${TOOLCHAIN}/lipo -create ${OPENSSL}-x86_64/lib/libssl.a ${OPENSSL}-armv7/lib/libssl.a ${OPENSSL}-arm64/lib/libssl.a -output ${PREFIX}/lib/libssl.a
${TOOLCHAIN}/lipo -create ${OPENSSL}-x86_64/lib/libcrypto.a ${OPENSSL}-armv7/lib/libcrypto.a ${OPENSSL}-arm64/lib/libcrypto.a -output ${PREFIX}/lib/libcrypto.a
mkdir -p ${PREFIX}/include
cp -R ${OPENSSL}-arm64/include ${PREFIX}/

export -n IOS_DEPLOYMENT_TARGET
export -n IOS_SYSROOT
export -n CROSS_COMPILE

cd $QTDIR
echo "Build library..."

#
# IOS sdk compiller support only c++14 dialect.
# So we make it consistent across all supported platforms.
#

`dirname $0`/src.git/configure -xplatform macx-ios-clang -prefix $PREFIX -c++std c++14 \
  -opensource -confirm-license -nomake examples -nomake tests -skip qtserialport -skip qtwebkit \
  -skip qtwebengine -skip speech -no-feature-geoservices_mapboxgl -no-feature-assimp \
  -openssl-linked -I $PREFIX/include -L $PREFIX/lib

make -j7
echo -e "\nall done."
